package buu.informatics.s59160342.thaitraffigsigns


import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59160342.thaitraffigsigns.databinding.FragmentMenuBinding

/**
 * A simple [Fragment] subclass.
 */
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class MenuFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentMenuBinding>(inflater,R.layout.fragment_menu,container,false)
        binding.btnRegulatory.setOnClickListener { view : View ->
            view.findNavController().navigate(R.id.action_menuFragment_to_regulatoryFragment)
        }
        binding.btnWarning.setOnClickListener { view: View ->
            view.findNavController().navigate(R.id.action_menuFragment_to_warningFragment)
        }
        binding.btnConstruction.setOnClickListener { view: View ->
            view.findNavController().navigate(R.id.action_menuFragment_to_constructionFragment)
        }
        binding.btnHighways.setOnClickListener { view: View ->
            view.findNavController().navigate(R.id.action_menuFragment_to_highwaysFragment)
        }
        binding.btnInformational.setOnClickListener { view: View ->
            view.findNavController().navigate(R.id.action_menuFragment_to_informationaFragment)
        }

        setHasOptionsMenu(true)
        return binding.root
    }

//    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        super.onCreateOptionsMenu(menu,inflater)
//        inflater.inflate(R.menu.options_menu,menu)
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return NavigationUI.onNavDestinationSelected(item!!,
//            view!!.findNavController())
//                || super.onOptionsItemSelected(item)
//    }
        override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.share -> shareSuccess()
        }
        return NavigationUI.onNavDestinationSelected(item,view!!.findNavController())
                ||super.onOptionsItemSelected(item)
    }
    //share
    private fun getShareIntent() : Intent {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_success_text  ))
        return shareIntent
    }
    private fun shareSuccess() {
        startActivity(getShareIntent())
    }
}
