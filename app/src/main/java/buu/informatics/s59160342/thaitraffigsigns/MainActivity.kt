package buu.informatics.s59160342.thaitraffigsigns

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59160342.thaitraffigsigns.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        @Suppress("UNUSED_VARIABLE")
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        drawerLayout = binding.drawerLayout

        val navController = this.findNavController(R.id.myNavHostFragment)

        NavigationUI.setupActionBarWithNavController(this,navController, drawerLayout)

        NavigationUI.setupWithNavController(binding.navView, navController)

        Log.i("MainActivity", "onCreate Called")
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.myNavHostFragment)
        return NavigationUI.navigateUp(navController, drawerLayout)
    }

    //  lifecycle
    override fun onStart() {
        super.onStart()
        Log.i("MainActivity", "onStart Called")
    }
    override fun onResume() {
        super.onResume()
            Log.i("MainActivity","onResume Called")
    }

    override fun onPause() {
        super.onPause()
            Log.i("MainActivity","onPause Called")
    }

    override fun onStop() {
        super.onStop()
            Log.i("MainActivity","onStop Called")
    }

    override fun onDestroy() {
        super.onDestroy()
            Log.i("MainActivity","onDestroy Called")
    }

    override fun onRestart() {
        super.onRestart()
            Log.i("MainActivity","onRestart Called")
    }
}

