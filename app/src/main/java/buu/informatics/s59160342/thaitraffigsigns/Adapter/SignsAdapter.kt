package buu.informatics.s59160342.thaitraffigsigns.Adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import buu.informatics.s59160342.thaitraffigsigns.DataClass.Signs
import buu.informatics.s59160342.thaitraffigsigns.DetailFragment
import buu.informatics.s59160342.thaitraffigsigns.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_sign.view.*
import kotlin.coroutines.coroutineContext


class SignsAdapter(private val list:List<Signs>,private val clickListener:(Signs) -> Unit):
    RecyclerView.Adapter<SignsAdapter.ViewHolder>(){

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position],clickListener)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder        {
       return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_sign, parent, false))
    }
    class ViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bind(item:Signs,clickListener: (Signs) -> Unit){
            itemView.apply {
                nameSign_length.text = item.name
            }
            Picasso.get().load(item.image).into(itemView.sign_imageage)
            itemView.setOnClickListener { clickListener(item)  }
        }

    }

}