package buu.informatics.s59160342.thaitraffigsigns


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import buu.informatics.s59160342.thaitraffigsigns.Adapter.SignsAdapter
import buu.informatics.s59160342.thaitraffigsigns.DataClass.Signs
import buu.informatics.s59160342.thaitraffigsigns.databinding.FragmentInformationaBinding
import kotlinx.android.synthetic.main.fragment_regulatory.*

/**
 * A simple [Fragment] subclass.
 */
class InformationaFragment : Fragment() {
    val listSign = listOf(
        Signs("Intersection name on Highways", "ชื่อแยกบนทางหลวง","https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Thailand_road_sign_%E0%B8%99-5-4.svg/100px-Thailand_road_sign_%E0%B8%99-5-4.svg.png"),
        Signs("Intersection name on Rural Roads", "ชื่อสี่แยกบนทางหลวงชนบท","https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Thailand_road_sign_%E0%B8%99-5-5.svg/100px-Thailand_road_sign_%E0%B8%99-5-5.svg.png"),
        Signs("Pedestrian crossing","ทางข้ามถนน","https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Thailand_road_sign_%E0%B8%99-6.svg/100px-Thailand_road_sign_%E0%B8%99-6.svg.png"),
        Signs("Hospital","โรงพยาบาล","https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Thailand_road_sign_%E0%B8%99-7.svg/100px-Thailand_road_sign_%E0%B8%99-7.svg.png"),
        Signs("One-way street (left)","ถนนเดินรถทางเดียว (ซ้าย)","https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Thailand_road_sign_%E0%B8%99-8-1.svg/100px-Thailand_road_sign_%E0%B8%99-8-1.svg.png"),
        Signs("One-way street (right)","ถนนเดินรถทางเดียว (ขวา)","https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Thailand_road_sign_%E0%B8%99-8-2.svg/100px-Thailand_road_sign_%E0%B8%99-8-2.svg.png")
    )
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentInformationaBinding>(inflater,R.layout.fragment_informationa,container,false)
        Toast.makeText(context, "Informational Page", Toast.LENGTH_LONG).show()
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val signAdapter = SignsAdapter(listSign,{listSign -> ItemClick(listSign)})
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            isNestedScrollingEnabled = false
            adapter =  signAdapter
            onFlingListener = null
        }
        signAdapter.notifyDataSetChanged()

    }
    private fun ItemClick(listSign: Signs){
        var name = listSign.name
        var detail = listSign.detail
        var image = listSign.image
        Toast.makeText(context,"Click :${detail}!!",Toast.LENGTH_LONG).show()
        findNavController().navigate(InformationaFragmentDirections.actionInformationaFragmentToDetailFragment(name,detail,image))
    }


}
