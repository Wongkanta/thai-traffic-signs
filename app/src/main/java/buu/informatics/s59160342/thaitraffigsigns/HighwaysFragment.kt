package buu.informatics.s59160342.thaitraffigsigns

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import buu.informatics.s59160342.thaitraffigsigns.Adapter.SignsAdapter
import buu.informatics.s59160342.thaitraffigsigns.DataClass.Signs
import buu.informatics.s59160342.thaitraffigsigns.databinding.FragmentHighwaysBinding
import kotlinx.android.synthetic.main.fragment_regulatory.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class HighwaysFragment : Fragment() {
    val listSign = listOf(
        Signs("Advance turn right","ป้ายเปลี่ยนทิศทางไปทางขวา","https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Thailand_road_sign_%E0%B8%99%E0%B8%AA-2.svg/750px-Thailand_road_sign_%E0%B8%99%E0%B8%AA-2.svg.png"),
        Signs("Advance turn left","ป้ายเปลี่ยนทิศทางไปทางซ้าย","https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Thailand_road_sign_%E0%B8%99%E0%B8%AA-1.svg/750px-Thailand_road_sign_%E0%B8%99%E0%B8%AA-1.svg.png"),
        Signs("Directional arrow signs (right)", "ป้ายลูกศรบอกทิศทาง (ขวา)","https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Thailand_road_sign_%E0%B8%99%E0%B8%AA-6.svg/750px-Thailand_road_sign_%E0%B8%99%E0%B8%AA-6.svg.png"),
        Signs("Directional arrow signs (left)","ป้ายลูกศรบอกทิศทาง (ซ้าย)","https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Thailand_road_sign_%E0%B8%99%E0%B8%AA-5.svg/750px-Thailand_road_sign_%E0%B8%99%E0%B8%AA-5.svg.png")
    )
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentHighwaysBinding>(inflater,R.layout.fragment_highways,container,false)
        Toast.makeText(context, "Highways Page", Toast.LENGTH_LONG).show()
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val signAdapter = SignsAdapter(listSign,{listSign -> ItemClick(listSign)})
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            isNestedScrollingEnabled = false
            adapter =  signAdapter
            onFlingListener = null
        }
        signAdapter.notifyDataSetChanged()

    }
    private fun ItemClick(listSign:Signs){
        var name = listSign.name
        var detail = listSign.detail
        var image = listSign.image
        Toast.makeText(context,"Click :${detail}!!",Toast.LENGTH_LONG).show()
        findNavController().navigate(HighwaysFragmentDirections.actionHighwaysFragmentToDetailFragment(name,detail,image))
    }




}
