package buu.informatics.s59160342.thaitraffigsigns


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import buu.informatics.s59160342.thaitraffigsigns.Adapter.SignsAdapter
import buu.informatics.s59160342.thaitraffigsigns.DataClass.Signs
import buu.informatics.s59160342.thaitraffigsigns.databinding.FragmentConstructionBinding
import kotlinx.android.synthetic.main.fragment_regulatory.*

/**
 * A simple [Fragment] subclass.
 */
class ConstructionFragment : Fragment() {
    val listSign = listOf(
        Signs("Diverted traffic to right","เบี่ยงเบนการจราจรไปทางขวา","https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Thailand_road_sign_%E0%B8%95%E0%B8%81-8.svg/120px-Thailand_road_sign_%E0%B8%95%E0%B8%81-8.svg.png"),
        Signs("Diverted traffic to left (two lanes)","เบี่ยงเบนการจราจรไปทางซ้าย (สองเลน)","https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Thailand_road_sign_%E0%B8%95%E0%B8%81-9.svg/120px-Thailand_road_sign_%E0%B8%95%E0%B8%81-9.svg.png"),
        Signs("Diverted traffic to right (two lanes)","เบี่ยงเบนการจราจรไปทางขวา (สองเลน)","https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Thailand_road_sign_%E0%B8%95%E0%B8%81-10.svg/120px-Thailand_road_sign_%E0%B8%95%E0%B8%81-10.svg.png"),
        Signs("Diverted traffic (one lane on left)","เปลี่ยนเส้นทางการจราจร (หนึ่งช่องทางซ้าย)","https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Thailand_road_sign_%E0%B8%95%E0%B8%81-11.svg/120px-Thailand_road_sign_%E0%B8%95%E0%B8%81-11.svg.png"),
        Signs("Diverted traffic (one lane on right)","เปลี่ยนเส้นทางการจราจร (หนึ่งเลนทางขวา)","https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Thailand_road_sign_%E0%B8%95%E0%B8%81-12.svg/120px-Thailand_road_sign_%E0%B8%95%E0%B8%81-12.svg.png")

    )
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentConstructionBinding>(inflater,R.layout.fragment_construction,container,false)
        Toast.makeText(context, "Construction Page", Toast.LENGTH_LONG).show()
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val signAdapter = SignsAdapter(listSign,{listSign -> ItemClick(listSign)})
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            isNestedScrollingEnabled = false
            adapter =  signAdapter
            onFlingListener = null
        }
        signAdapter.notifyDataSetChanged()

    }
    private fun ItemClick(listSign:Signs){
        var name = listSign.name
        var detail = listSign.detail
        var image = listSign.image
        Toast.makeText(context,"Click :${detail}!!",Toast.LENGTH_LONG).show()
        findNavController().navigate(ConstructionFragmentDirections.actionConstructionFragmentToDetailFragment(name,detail,image))
    }

}
