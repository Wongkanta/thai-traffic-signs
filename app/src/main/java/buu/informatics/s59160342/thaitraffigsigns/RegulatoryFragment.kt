package buu.informatics.s59160342.thaitraffigsigns


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import buu.informatics.s59160342.thaitraffigsigns.Adapter.SignsAdapter
import buu.informatics.s59160342.thaitraffigsigns.DataClass.Signs
import buu.informatics.s59160342.thaitraffigsigns.databinding.FragmentRegulatoryBinding
import kotlinx.android.synthetic.main.fragment_regulatory.*

/**
 * A simple [Fragment] subclass.
 */
class RegulatoryFragment : Fragment() {

    val listSign = listOf(
                Signs("Stop"," รถทุกชนิดต้องหยุด เมื่อเห็นว่าปลอดภัยแล้ว จึงให้เคลื่อนรถต่อไปได้ด้วยความระมัดระวัง","https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Thailand_road_sign_บ-1.svg/800px-Thailand_road_sign_บ-1.svg.png"),
                Signs("No U-turn to the right.","ห้ามมิให้กลับรถไปทางขวาไม่ว่าด้วยวิธีใดๆ  ในเขตทางที่ติดตั้งป้าย","https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Thailand_road_sign_%E0%B8%9A-6.svg/600px-Thailand_road_sign_%E0%B8%9A-6.svg.png"),
                Signs("Give way","รถทุกชนิดต้องระมัดระวังและให้ทางแก่รถและคนเดินเท้าในทางขวางหน้าผ่านไป ก่อน เมื่อเห็นว่าปลอดภัย และ ไม่เป็นการกีดขวางการจราจรที่บริเวณทางแยกนั้นแล้ว  จึงให้เคลื่อนรถต่อไปได้ด้วยความระมัดระวัง\n","https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Thailand_road_sign_%E0%B8%9A-2.svg/677px-Thailand_road_sign_%E0%B8%9A-2.svg.png"),
                Signs("Give way to oncoming traffic.","ให้ผู้ขับรถทุกชนิดหยุดรถตรงป้าย เพื่อให้รถที่กำลังแล่นสวนทางมาก่อน ถ้ามีรถข้างหน้าหยุดรออยู่ก่อนก็ให้หยุดรถรอถัดต่อกันมาตามลำดับ เมื่อรถที่สวนทางมาได้ผ่านไปหมดแล้ว จึงให้รถที่หยุดรอตามป้ายนี้เคลื่อนไปได้","https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Thailand_road_sign_%E0%B8%9A-3.svg/600px-Thailand_road_sign_%E0%B8%9A-3.svg.png"),
                Signs("No overtaking","ห้ามมิให้ขับรถแซงขึ้นหน้ารถคันอื่นในเขตทางที่ติดตั้งป้าย","https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Thailand_road_sign_%E0%B8%9A-4.svg/600px-Thailand_road_sign_%E0%B8%9A-4.svg.png"),
                Signs("Speed limit (50 km/h).","ห้ามมิให้ผู้ขับรถทุกชนิดใช้ความเร็วเกินกว่าที่กำหนดเป็นกิโลเมตรต่อชั่วโมง ตามจำนวนตัวเลขในแผ่นป้ายนั้นๆ ในเขตทางที่ติดตั้งป้าย จนกว่าจะพ้นที่สุดระยะที่จำกัดความเร็วนั้น\n","https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Thailand_road_sign_%E0%B8%9A-32-50.svg/600px-Thailand_road_sign_%E0%B8%9A-32-50.svg.png")
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentRegulatoryBinding>(inflater,R.layout.fragment_regulatory,container,false)
        Toast.makeText(context, "Regulatory Page", Toast.LENGTH_LONG).show()
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val signAdapter = SignsAdapter(listSign,{listSign -> ItemClick(listSign)})
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            isNestedScrollingEnabled = false
            adapter =  signAdapter
            onFlingListener = null
        }
        signAdapter.notifyDataSetChanged()

    }
    private fun ItemClick(listSign:Signs){
        var name = listSign.name
        var detail = listSign.detail
        var image = listSign.image
        Toast.makeText(context,"Click :${detail}!!",Toast.LENGTH_LONG).show()
        findNavController().navigate(RegulatoryFragmentDirections.actionRegulatoryFragmentToDetailFragment(name,detail,image))
    }


}
