package buu.informatics.s59160342.thaitraffigsigns


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import buu.informatics.s59160342.thaitraffigsigns.databinding.FragmentAboutBinding

/**
 * A simple [Fragment] subclass.
 */
class aboutFragment : Fragment() {

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle? ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentAboutBinding>(inflater,R.layout.fragment_about,container,false)
        Toast.makeText(context, "About Page", Toast.LENGTH_LONG).show()
        return binding.root
    }


}
