package buu.informatics.s59160342.thaitraffigsigns


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import buu.informatics.s59160342.thaitraffigsigns.Adapter.SignsAdapter
import buu.informatics.s59160342.thaitraffigsigns.DataClass.Signs
import buu.informatics.s59160342.thaitraffigsigns.databinding.FragmentWarningBinding
import kotlinx.android.synthetic.main.fragment_regulatory.*

/**
 * A simple [Fragment] subclass.
 */
class WarningFragment : Fragment() {
    val listSign = listOf(
        Signs("Curve to left","ทางข้างหน้าโค้งไปทางซ้าย ให้ขับรถให้ช้าลงพอสมควรและเดินรถชิดด้านซ้ายด้วยความระมัดระวัง","https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Thailand_road_sign_%E0%B8%95-1.svg/600px-Thailand_road_sign_%E0%B8%95-1.svg.png"),
        Signs("Curve to right","ทางข้างหน้าโค้งไปทางขวา ให้ขับรถให้ช้าลงพอสมควรและเดินรถชิดด้านขวาด้วยความระมัดระวัง","https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Thailand_road_sign_%E0%B8%95-2.svg/120px-Thailand_road_sign_%E0%B8%95-2.svg.png"),
        Signs("Sharp curve to left","ทางข้างหน้าโค้งรัศมีแคบไปทางซ้าย ให้ขับรถให้ช้าลงพอสมควรและเดินรถชิดด้านซ้ายด้วยความระมัดระวัง","https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Thailand_road_sign_%E0%B8%95-3.svg/120px-Thailand_road_sign_%E0%B8%95-3.svg.png"),
        Signs("Sharp curve to right","ทางข้างหน้าโค้งรัศมีแคบไปทางขวา ให้ขับรถให้ช้าลงพอสมควรและเดินรถชิดด้านขวาด้วยความระมัดระวัง","https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Thailand_road_sign_%E0%B8%95-4.svg/120px-Thailand_road_sign_%E0%B8%95-4.svg.png"),
        Signs("Double curve, first to left","ทางข้างหน้าโค้งรัศมีแคบไปทางซ้ายแล้วกลับ ให้ขับรถให้ช้าลงพอสมควรและเดินรถชิดด้านขวาด้วยความระมัดระวัง","https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Thailand_road_sign_%E0%B8%95-5.svg/120px-Thailand_road_sign_%E0%B8%95-5.svg.png"),
        Signs("Double curve, first to right","ทางข้างหน้าโค้งรัศมีแคบไปทางขวาแล้วกลับ ให้ขับรถให้ช้าลงพอสมควรและเดินรถชิดด้านขวาด้วยความระมัดระวัง","https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Thailand_road_sign_%E0%B8%95-6.svg/120px-Thailand_road_sign_%E0%B8%95-6.svg.png")
    )
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentWarningBinding>(inflater,R.layout.fragment_warning,container,false)
        Toast.makeText(context, "Warning Page", Toast.LENGTH_LONG).show()
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val signAdapter = SignsAdapter(listSign,{listSign -> ItemClick(listSign)})
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            isNestedScrollingEnabled = false
            adapter =  signAdapter
            onFlingListener = null
        }
        signAdapter.notifyDataSetChanged()

    }
    private fun ItemClick(listSign:Signs){
        var name = listSign.name
        var detail = listSign.detail
        var image = listSign.image
        Toast.makeText(context,"Click :${detail}!!",Toast.LENGTH_LONG).show()
        findNavController().navigate(WarningFragmentDirections.actionWarningFragmentToDetailFragment(name,detail,image))
    }
}
