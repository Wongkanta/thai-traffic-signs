package buu.informatics.s59160342.thaitraffigsigns


import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59160342.thaitraffigsigns.databinding.FragmentDetailBinding
import com.squareup.picasso.Picasso

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentDetailBinding>(inflater,R.layout.fragment_detail,container,false)
//        val args = GameWonFragmentArgs.fromBundle(arguments!!)
//        Toast.makeText(context, "${args.name}", Toast.LENGTH_LONG).show()

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = DetailFragmentArgs.fromBundle(arguments!!)
        Toast.makeText(context, "${args.nameSign}", Toast.LENGTH_LONG).show()
        val nameTextView = view.findViewById<TextView>(R.id.textView_name)
        val detailTextView = view.findViewById<TextView>(R.id.textView_detail)
        val imageView = view.findViewById<ImageView>(R.id.imageView_Sign)
        nameTextView.text = args.nameSign.toString()
        detailTextView.text = args.detailSign
        Picasso.get().load(args.imageSign).into(imageView)
    }
    private fun getShareIntent() : Intent {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_success_text  ))
        return shareIntent
    }
    private fun shareSuccess() {
        startActivity(getShareIntent())
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.share -> shareSuccess()
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu,menu)
    }


}
